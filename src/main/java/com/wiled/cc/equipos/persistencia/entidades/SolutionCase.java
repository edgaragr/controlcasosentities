/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.Cacheable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author edgar
 */
@Entity
@Cacheable(false)
public class SolutionCase implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToMany(mappedBy = "solutionCase", cascade=CascadeType.ALL, fetch=FetchType.LAZY)
    private List<ReparacionDispensador> dispensadores = new ArrayList<ReparacionDispensador>();

    @OneToMany(mappedBy = "solutionCase", cascade=CascadeType.ALL)
    private List<ReparacionBomba> bombas = new ArrayList<ReparacionBomba>();

    @OneToMany(mappedBy = "solutionCase", cascade=CascadeType.ALL)
    private List<ReparacionElevador> elevadores = new ArrayList<ReparacionElevador>();

    @OneToMany(mappedBy = "solutionCase", cascade=CascadeType.ALL)
    private List<ReparacionMarquesina> marquesinas = new ArrayList<ReparacionMarquesina>();

    @OneToMany(mappedBy = "solutionCase", cascade=CascadeType.ALL)
    private List<ReparacionCompensador> compensadores = new ArrayList<ReparacionCompensador>();

    @OneToMany(mappedBy = "solutionCase", cascade=CascadeType.ALL)
    private List<ReparacionMedidor> medidores = new ArrayList<ReparacionMedidor>();

    @OneToMany(mappedBy = "solutionCase", cascade=CascadeType.ALL)
    private List<ReparacionSurtidor> surtidores = new ArrayList<ReparacionSurtidor>();
    
    @OneToMany(mappedBy = "solutionCase", cascade=CascadeType.ALL)
    private List<ReparacionTanque> tanques = new ArrayList<ReparacionTanque>();

    @OneToOne
    private StationCase station;
    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date closeDate;
    private String comentarioCierre;

    public String getComentarioCierre() {
        return comentarioCierre;
    }

    public void setComentarioCierre(String comentarioCierre) {
        this.comentarioCierre = comentarioCierre;
    }


    public List<ReparacionSurtidor> getSurtidores() {
        return surtidores;
    }

    public void setSurtidores(List<ReparacionSurtidor> surtidores) {
        this.surtidores = surtidores;
    }

    public List<ReparacionTanque> getTanques() {
        return tanques;
    }

    public void setTanques(List<ReparacionTanque> tanques) {
        this.tanques = tanques;
    }

    public List<ReparacionMedidor> getMedidores() {
        return medidores;
    }

    public void setMedidores(List<ReparacionMedidor> medidores) {
        this.medidores = medidores;
    }

    public List<ReparacionCompensador> getCompensadores() {
        return compensadores;
    }

    public void setCompensadores(List<ReparacionCompensador> compensadores) {
        this.compensadores = compensadores;
    }

    public List<ReparacionMarquesina> getMarquesinas() {
        return marquesinas;
    }

    public void setMarquesinas(List<ReparacionMarquesina> marquesinas) {
        this.marquesinas = marquesinas;
    }

    
    public List<ReparacionBomba> getBombas() {
        return bombas;
    }

    public void setBombas(List<ReparacionBomba> bombas) {
        this.bombas = bombas;
    }

    public List<ReparacionElevador> getElevadores() {
        return elevadores;
    }

    public void setElevadores(List<ReparacionElevador> elevadores) {
        this.elevadores = elevadores;
    }

    
    public List<ReparacionDispensador> getDispensadores() {
        return dispensadores;
    }

    public void setDispensadores(List<ReparacionDispensador> dispensadores) {
        this.dispensadores = dispensadores;
    }

    public Date getCloseDate() {
        return closeDate;
    }

    public void setCloseDate(Date closeDate) {
        this.closeDate = closeDate;
    }

    public StationCase getStation() {
        return station;
    }

    public void setStation(StationCase station) {
        this.station = station;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof SolutionCase)) {
            return false;
        }
        SolutionCase other = (SolutionCase) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "imirca.equipos.persistencia.entidades.SolutionCase[id=" + id + "]";
    }

}
