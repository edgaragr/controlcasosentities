/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author edgar
 */
@Entity
@NamedQueries({
    @NamedQuery(name="getStationsByName", query="select o from Station o where o.name =:name"),
    @NamedQuery(name="getStationsByType", query="select o from Station o where o.type =:type"),
    @NamedQuery(name="getStationsByZone", query="select o from Station o where o.zone =:zone"),
    @NamedQuery(name="getStationByCompany", query="select o from Station o where o.company =:company")
})

public class Station implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Company company;
    private String name;
    private String number;
    private String telephone;
    private String address;
    @OneToOne
    private StationZone zone;
    @OneToOne
    private StationType type;
    private String manager;
    private String email;
    @OneToMany(mappedBy = "station", cascade=CascadeType.ALL)
    private List<Tanque> tanques = new ArrayList<Tanque>();

    @OneToMany(mappedBy = "station", cascade=CascadeType.ALL)
    private List<BombaSumergible> bombas = new ArrayList<BombaSumergible>();

    @OneToMany(mappedBy = "station", cascade=CascadeType.ALL)
    private List<Compensador> compensadores = new ArrayList<Compensador>();

    @OneToMany(mappedBy = "station", cascade=CascadeType.ALL)
    private List<Dispensador> dispensadores = new ArrayList<Dispensador>();
    
    @OneToMany(mappedBy = "station", cascade=CascadeType.ALL)
    private List<Elevador> elevadores = new ArrayList<Elevador>();

    @OneToMany(mappedBy = "station", cascade=CascadeType.ALL)
    private List<Marquesina> marquesinas = new ArrayList<Marquesina>();

    @OneToMany(mappedBy = "station", cascade=CascadeType.ALL)
    private List<MedidorAire> medidoresAire = new ArrayList<MedidorAire>();

    @OneToMany(mappedBy = "station", cascade=CascadeType.ALL)
    private List<Surtidor> surtidores = new ArrayList<Surtidor>();

    @OneToMany(mappedBy = "station", cascade=CascadeType.ALL)
    private List<StationCase> casos;

    @Lob
    private byte[] picture;

    @OneToMany(mappedBy = "station")
    private List<InventarioPiezas> inventarios = new ArrayList<InventarioPiezas>();

    @ElementCollection
    @Temporal(TemporalType.DATE)
    private List<Date> fechasMantenimientosPreventivos = new ArrayList<Date>();

    public List<Date> getFechasMantenimientosPreventivos() {
        return fechasMantenimientosPreventivos;
    }

    public void setFechasMantenimientosPreventivos(List<Date> fechasMantenimientosPreventivos) {
        this.fechasMantenimientosPreventivos = fechasMantenimientosPreventivos;
    }

    public List<InventarioPiezas> getInventarios() {
        return inventarios;
    }

    public void setInventarios(List<InventarioPiezas> inventarios) {
        this.inventarios = inventarios;
    }

    public byte[] getPicture() {
        return picture;
    }

    public void setPicture(byte[] picture) {
        this.picture = picture;
    }

    
    public List<Surtidor> getSurtidores() {
        return surtidores;
    }

    public void setSurtidores(List<Surtidor> surtidores) {
        this.surtidores = surtidores;
    }

    public List<MedidorAire> getMedidoresAire() {
        return medidoresAire;
    }

    public void setMedidoresAire(List<MedidorAire> medidoresAire) {
        this.medidoresAire = medidoresAire;
    }

    public List<Elevador> getElevadores() {
        return elevadores;
    }

    public List<Marquesina> getMarquesinas() {
        return marquesinas;
    }

    public void setMarquesinas(List<Marquesina> marquesinas) {
        this.marquesinas = marquesinas;
    }

    public void setElevadores(List<Elevador> elevadores) {
        this.elevadores = elevadores;
    }

    public List<Dispensador> getDispensadores() {
        return dispensadores;
    }

    public void setDispensadores(List<Dispensador> dispensadores) {
        this.dispensadores = dispensadores;
    }

    
    public List<Compensador> getCompensadores() {
        return compensadores;
    }

    public void setCompensadores(List<Compensador> compensadores) {
        this.compensadores = compensadores;
    }

    public List<BombaSumergible> getBombas() {
        return bombas;
    }

    public void setBombas(List<BombaSumergible> bombas) {
        this.bombas = bombas;
    }

    public List<StationCase> getCasos() {
        return casos;
    }

    public void setCasos(List<StationCase> casos) {
        this.casos = casos;
    }

    public List<Tanque> getTanques() {
        return tanques;
    }

    public void setTanques(List<Tanque> tanques) {
        this.tanques = tanques;
    }

    public Company getCompany() {
        return company;
    }

    public void setCompany(Company company) {
        this.company = company;
    }
    
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getManager() {
        return manager;
    }

    public void setManager(String manager) {
        this.manager = manager;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public StationType getType() {
        return type;
    }

    public void setType(StationType type) {
        this.type = type;
    }

    public StationZone getZone() {
        return zone;
    }

    public void setZone(StationZone zone) {
        this.zone = zone;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Station)) {
            return false;
        }
        Station other = (Station) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return name;
    }
}
