/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.io.Serializable;
import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

/**
 *
 * @author edgar
 */
@Entity
public class Marquesina implements Serializable{
    @OneToMany(mappedBy = "marquesina", cascade=CascadeType.PERSIST)
    private List<MarquesinaProductEntry> productos;

    @ManyToOne
    private Station station;
    
    @OneToMany(mappedBy = "marquesina")
    @ManyToMany(mappedBy = "marquesina")
    private Collection<StationCase> casos;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    @OneToMany(mappedBy = "marquesina")
    private List<ReparacionMarquesina> reparaciones;

    public List<ReparacionMarquesina> getReparaciones() {
        return reparaciones;
    }

    public void setReparaciones(List<ReparacionMarquesina> reparaciones) {
        this.reparaciones = reparaciones;
    }
    
    @PrePersist
    public void setMarquesinaReference() {
        for(MarquesinaProductEntry entry : productos) {
            entry.setMarquesina(this);
        }
    }

    public Collection<StationCase> getCasos() {
        return casos;
    }

    public void setCasos(Collection<StationCase> casos) {
        this.casos = casos;
    }
    
    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public List<MarquesinaProductEntry> getProductos() {
        return productos;
    }

    public void setProductos(List<MarquesinaProductEntry> productos) {
        this.productos = productos;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Equipment)) {
            return false;
        }
        Marquesina other = (Marquesina) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }
}
