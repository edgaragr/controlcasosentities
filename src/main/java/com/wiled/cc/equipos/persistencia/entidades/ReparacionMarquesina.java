/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author edgar
 */
@Entity
@NamedQueries({
    @NamedQuery(name="buscarReparacionMarquesina", query="select o from ReparacionMarquesina o where o.marquesina =:marquesina and o.pieza =:pieza and o.cantidad =:cantidad"),
    @NamedQuery(name="getMarquesinaPiezaSum", query="select SUM(r.cantidad) from ReparacionMarquesina r where r.fechaReparacion >= :fecha and r.pieza =:pieza")
})
public class ReparacionMarquesina implements Serializable {
    @ManyToOne
    private SolutionCase solutionCase;
    @ManyToOne
    private Marquesina marquesina;
    @OneToOne
    private Pieza pieza;
    private int cantidad;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaReparacion;

    public Date getFechaReparacion() {
        return fechaReparacion;
    }

    public void setFechaReparacion(Date fechaReparacion) {
        this.fechaReparacion = fechaReparacion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Pieza getPieza() {
        return pieza;
    }

    public void setPieza(Pieza pieza) {
        this.pieza = pieza;
    }
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Marquesina getMarquesina() {
        return marquesina;
    }

    public void setMarquesina(Marquesina marquesina) {
        this.marquesina = marquesina;
    }

    public SolutionCase getSolutionCase() {
        return solutionCase;
    }

    public void setSolutionCase(SolutionCase solutionCase) {
        this.solutionCase = solutionCase;
    }


    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReparacionMarquesina)) {
            return false;
        }
        ReparacionMarquesina other = (ReparacionMarquesina) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "imirca.equipos.persistencia.entidades.ReparacionMarquesina[id=" + id + "]";
    }

}
