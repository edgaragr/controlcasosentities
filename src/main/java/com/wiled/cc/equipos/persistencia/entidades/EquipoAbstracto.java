/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.Temporal;

/**
 *
 * @author egarcia
 */
@MappedSuperclass
@Inheritance(strategy=InheritanceType.JOINED)
public abstract class EquipoAbstracto implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Long id;
    @Column(unique=true, nullable=false)
    protected String serial;
    protected String marca;
    protected String modelo;
    @Column(nullable=false)
    @Temporal(javax.persistence.TemporalType.DATE)
    protected Date fechaGarantia;
    protected int reparacionesContratadas;

    public int getReparacionesContratadas() {
        return reparacionesContratadas;
    }

    public void setReparacionesContratadas(int reparacionesContratadas) {
        this.reparacionesContratadas = reparacionesContratadas;
    }

    public Date getFechaGarantia() {
        return fechaGarantia;
    }

    public void setFechaGarantia(Date fechaGarantia) {
        this.fechaGarantia = fechaGarantia;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }

    public String getSerial() {
        return serial;
    }

    public void setSerial(String serial) {
        this.serial = serial;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
    
    @Override
    public String toString() {
        return getSerial();
    }
}
