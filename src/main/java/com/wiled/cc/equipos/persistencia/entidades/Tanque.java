/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author edgar
 */
@Entity
@NamedQuery(name="buscarTanqueSerial", query="select o from Tanque o where o.serial =:serial and o.station =:station")
public class Tanque extends EquipoAbstracto{
    @ManyToOne
    private Station station;
    @OneToOne
    private Product producto;  
    @ManyToMany(mappedBy = "tanque")
    private List<StationCase> casos;
    @OneToMany(mappedBy = "tanque")
    private List<ReparacionTanque> reparaciones;

    public List<ReparacionTanque> getReparaciones() {
        return reparaciones;
    }

    public void setReparaciones(List<ReparacionTanque> reparaciones) {
        this.reparaciones = reparaciones;
    }
    
    public Product getProducto() {
        return producto;
    }

    public void setProducto(Product producto) {
        this.producto = producto;
    }

    public List<StationCase> getCasos() {
        return casos;
    }

    public void setCasos(List<StationCase> casos) {
        this.casos = casos;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Station getStation() {
        return station;
    }
}
