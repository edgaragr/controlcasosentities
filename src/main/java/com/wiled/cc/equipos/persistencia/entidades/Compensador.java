/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.util.Collection;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author edgar
 */
@Entity
@NamedQuery(name="buscarCompensadorSerial", query="select o from Compensador o where o.serial =:serial and o.station =:station")
public class Compensador extends EquipoAbstracto{
    @ManyToOne
    private Station station;
    @ManyToMany(mappedBy = "compensador")
    private Collection<StationCase> casos;

    @OneToMany(mappedBy = "compensador")
    private List<ReparacionCompensador> reparaciones;

    public List<ReparacionCompensador> getReparaciones() {
        return reparaciones;
    }

    public void setReparaciones(List<ReparacionCompensador> reparaciones) {
        this.reparaciones = reparaciones;
    }

    
    public Collection<StationCase> getCasos() {
        return casos;
    }

    public void setCasos(Collection<StationCase> casos) {
        this.casos = casos;
    }

    private String hp;

    public String getHp() {
        return hp;
    }

    public void setHp(String hp) {
        this.hp = hp;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }
}
