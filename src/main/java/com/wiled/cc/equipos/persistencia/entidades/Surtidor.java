/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

/**
 *
 * @author edgar
 */
@Entity
@NamedQueries({
    @NamedQuery(name="buscarSurtidorSerial", query="select o from Surtidor o where o.serial =:serial and o.station =:station"),
    @NamedQuery(name="buscarSurtidorSerialSinEstacion", query="select o from Surtidor o where o.serial =:serial")
})

public class Surtidor extends EquipoAbstracto{
    @ManyToOne
    private Station station;
    @ManyToMany(mappedBy = "surtidor")
    protected Collection<StationCase> casos;
    @OneToMany(mappedBy = "surtidor", cascade = CascadeType.PERSIST)
    private List<SurtidorProductEntry> products;
    @OneToMany(mappedBy = "surtidor")
    private List<ReparacionSurtidor> reparaciones;

    public List<ReparacionSurtidor> getReparaciones() {
        return reparaciones;
    }

    public void setReparaciones(List<ReparacionSurtidor> reparaciones) {
        this.reparaciones = reparaciones;
    }
    
    public Collection<StationCase> getCasos() {
        return casos;
    }

    public void setCasos(Collection<StationCase> casos) {
        this.casos = casos;
    }

    public List<SurtidorProductEntry> getProducts() {
        return products;
    }

    public void setProducts(List<SurtidorProductEntry> products) {
        this.products = products;
    }

    public Station getStation() {
        return station;
    }

    @PrePersist
    public void setSurtidorReference() {
        for(SurtidorProductEntry entry : products) {
            entry.setSurtidor(this);
        }
    }

    public void setStation(Station station) {
        this.station = station;
    }
}
