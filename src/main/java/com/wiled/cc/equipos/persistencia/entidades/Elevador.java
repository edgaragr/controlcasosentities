/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author edgar
 */
@Entity
@NamedQuery(name="buscarElevadorSerial", query="select o from Elevador o where o.serial =:serial and o.station =:station")
public class Elevador extends EquipoAbstracto {
    @ManyToOne
    private Station station;
    @ManyToMany(mappedBy = "elevador")
    protected List<StationCase> casos;

    @OneToMany(mappedBy = "elevador")
    private List<ReparacionElevador> reparaciones;

    public List<ReparacionElevador> getReparaciones() {
        return reparaciones;
    }

    public void setReparaciones(List<ReparacionElevador> reparaciones) {
        this.reparaciones = reparaciones;
    }

    
    public List<StationCase> getCasos() {
        return casos;
    }

    public void setCasos(List<StationCase> casos) {
        this.casos = casos;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }
}
