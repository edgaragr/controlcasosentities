/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;

/**
 *
 * @author edgar
 */
@Entity
@NamedQuery(name="buscarMedidorAireSerial", query="select o from MedidorAire o where o.serial =:serial and o.station =:station")
public class MedidorAire extends EquipoAbstracto{
    @ManyToOne
    private Station station;
    @ManyToMany(mappedBy = "medidorAire")
    protected List<StationCase> casos;
    @OneToMany(mappedBy = "medidorAire")
    private List<ReparacionMedidor> reparaciones;

    public List<ReparacionMedidor> getReparaciones() {
        return reparaciones;
    }

    public void setReparaciones(List<ReparacionMedidor> reparaciones) {
        this.reparaciones = reparaciones;
    }

    public List <StationCase> getCasos() {
        return casos;
    }

    public void setCasos(List<StationCase> casos) {
        this.casos = casos;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    
}
