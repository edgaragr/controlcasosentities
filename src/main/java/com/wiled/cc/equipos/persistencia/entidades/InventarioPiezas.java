/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;
import javax.persistence.Temporal;

/**
 *
 * @author egarcia
 */
@Entity
public class InventarioPiezas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @OneToMany(mappedBy = "inventarioPiezas", cascade=CascadeType.PERSIST)
    private List<InventarioPiezaEntry> piezas = new ArrayList<InventarioPiezaEntry>();

    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaCreacion;

    @ManyToOne
    private Station station;

    @PrePersist
    public void setInventarioReference() {
        for(InventarioPiezaEntry entry : piezas) {
            entry.setInventarioPiezas(this);
        }
    }

    public Station getStation() {
        return station;
    }

    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public List<InventarioPiezaEntry> getPiezas() {
        return piezas;
    }

    public void setPiezas(List<InventarioPiezaEntry> piezas) {
        this.piezas = piezas;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof InventarioPiezas)) {
            return false;
        }
        InventarioPiezas other = (InventarioPiezas) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "controlcasosimirca.persistence.entities.InventarioPiezas[id=" + id + "]";
    }

}
