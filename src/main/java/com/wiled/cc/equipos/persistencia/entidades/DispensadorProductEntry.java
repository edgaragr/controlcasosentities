/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class DispensadorProductEntry extends AbstractProductEntry implements Serializable {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Dispensador dispensador;

    public Dispensador getDispensador() {
        return dispensador;
    }

    public void setDispensador(Dispensador dispensador) {
        this.dispensador = dispensador;
    }

    public DispensadorProductEntry(Dispensador dispensador) {
        this.dispensador = dispensador;
    }

    public DispensadorProductEntry() {
    }
    
    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof DispensadorProductEntry)) {
            return false;
        }
        DispensadorProductEntry other = (DispensadorProductEntry) object;

        if(this.getProduct().getName().equalsIgnoreCase(other.getProduct().getName())) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
