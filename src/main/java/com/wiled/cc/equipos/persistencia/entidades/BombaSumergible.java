/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.util.Collection;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

/**
 *
 * @author edgar
 */
@Entity
@NamedQuery(name="buscarBombaSerial", query="select o from BombaSumergible o where o.serial =:serial and o.station =:station")
public class BombaSumergible extends EquipoAbstracto{
    @ManyToOne
    private Station station;
    @ManyToMany(mappedBy = "bombaSumergible")
    protected Collection<StationCase> casos;

    @OneToMany(mappedBy = "bombaSumergible")
    private List<ReparacionBomba> reparaciones;

    @OneToOne
    private Product product;

    public List<ReparacionBomba> getReparaciones() {
        return reparaciones;
    }

    public void setReparaciones(List<ReparacionBomba> reparaciones) {
        this.reparaciones = reparaciones;
    }

    public Collection<StationCase> getCasos() {
        return casos;
    }

    public void setCasos(Collection<StationCase> casos) {
        this.casos = casos;
    }

    public Product getProduct() {
        return product;
    }

    public void setProduct(Product product) {
        this.product = product;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }
}
