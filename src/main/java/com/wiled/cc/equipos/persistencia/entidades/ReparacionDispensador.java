/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Cacheable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;

/**
 *
 * @author edgar
 */
@Entity
@NamedQueries({
    @NamedQuery(name="buscarReparacionDispensador", query="select o from ReparacionDispensador o where o.dispensador =:dispensador and o.pieza =:pieza and o.cantidad =:cantidad"),
    @NamedQuery(name="getDispensadorPiezaSum", query="select SUM(r.cantidad) from ReparacionDispensador r where r.fechaReparacion >= :fecha and r.pieza =:pieza")
})
@Cacheable(false)
public class ReparacionDispensador implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    private Dispensador dispensador;
    @ManyToOne
    private SolutionCase solutionCase;
    @OneToOne
    private Pieza pieza;
    private int cantidad;
    @Temporal(javax.persistence.TemporalType.DATE)
    private Date fechaReparacion;

    public Date getFechaReparacion() {
        return fechaReparacion;
    }

    public void setFechaReparacion(Date fechaReparacion) {
        this.fechaReparacion = fechaReparacion;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public Pieza getPieza() {
        return pieza;
    }

    public void setPieza(Pieza pieza) {
        this.pieza = pieza;
    }

    public SolutionCase getSolutionCase() {
        return solutionCase;
    }

    public void setSolutionCase(SolutionCase solutionCase) {
        this.solutionCase = solutionCase;
    }
    


    public Dispensador getDispensador() {
        return dispensador;
    }

    public void setDispensador(Dispensador dispensador) {
        this.dispensador = dispensador;
    }

    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof ReparacionDispensador)) {
            return false;
        }
        ReparacionDispensador other = (ReparacionDispensador) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "imirca.equipos.persistencia.entidades.ReparacionDispensador[id=" + id + "]";
    }

}
