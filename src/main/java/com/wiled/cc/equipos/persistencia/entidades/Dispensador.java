/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.util.Collection;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.PrePersist;

/**
 *
 * @author edgar
 */
@Entity
@NamedQueries({
    @NamedQuery(name="buscarDispensadorSerial", query="select o from Dispensador o where o.serial =:serial and o.station =:station"),
    @NamedQuery(name="buscarDispensadorSerialSinEstacion", query="select o from Dispensador o where o.serial =:serial")
})

public class Dispensador extends EquipoAbstracto{
    @ManyToOne
    private Station station;
    @ManyToMany(mappedBy = "dispensador")
    protected Collection<StationCase> casos;
    @OneToMany(mappedBy = "dispensador", cascade = CascadeType.PERSIST)
    private List<DispensadorProductEntry> products;
    @OneToMany(mappedBy = "dispensador")
    private List<ReparacionDispensador> reparaciones;

    public List<ReparacionDispensador> getReparaciones() {
        return reparaciones;
    }

    public void setReparaciones(List<ReparacionDispensador> reparaciones) {
        this.reparaciones = reparaciones;
    }

    
    public Collection<StationCase> getCasos() {
        return casos;
    }

    public void setCasos(Collection<StationCase> casos) {
        this.casos = casos;
    }


    public List<DispensadorProductEntry> getProducts() {
        return products;
    }

    public void setProducts(List<DispensadorProductEntry> products) {
        this.products = products;
    }

    @PrePersist
    public void setDispensadorReference() {
        for(DispensadorProductEntry entry : products) {
            entry.setDispensador(this);
        }
    }
    
    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }


}
