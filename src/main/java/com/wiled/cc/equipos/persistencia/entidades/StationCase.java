/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author egarcia
 */
@Entity
@NamedQueries({
    @NamedQuery(name="BuscarCasoPorNumero", query="select o from StationCase o where o.numeroCaso =:numeroCaso"),
    @NamedQuery(name="BuscarCasoPorCompania", query="select o from StationCase o where o.station.company =:company"),
    @NamedQuery(name="BuscarCasoPorCategoria", query="select o from StationCase o where o.categoria =:categoria"),
    @NamedQuery(name="BuscarCasoPorStatus", query="select o from StationCase o where o.statusCase =:statusCase"),
    @NamedQuery(name="BuscarCasoPorCompaniaStatus", query="select o from StationCase o where o.station.company =:company and o.statusCase =:statusCase")
})
public class StationCase implements Serializable {
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    
    @ManyToOne
    private Station station;

    private boolean shell;
    
    @ManyToMany
    private List<Tanque> tanque;

    @ManyToMany
    private List<Surtidor> surtidor;

    @ManyToMany
    private List<MedidorAire> medidorAire;

    @ManyToMany
    private List<Marquesina> marquesina;

    @ManyToMany
    private List<Elevador> elevador;

    @ManyToMany
    private List<Dispensador> dispensador;

    @ManyToMany
    private List<Compensador> compensador;

    @ManyToMany
    private List<BombaSumergible> bombaSumergible;

    @OneToOne
    private CaseStatus statusCase;
    @ManyToOne
    private Technic technic;

    @OneToMany(mappedBy = "stationCase")
    private List<CaseComment> comentarios = new ArrayList<CaseComment>();

    @JoinColumn(nullable=true)
    @OneToOne(mappedBy = "station", cascade=CascadeType.ALL)
    private SolutionCase solutionCase;

    @Temporal(javax.persistence.TemporalType.TIMESTAMP)
    private Date createdDate;

    @OneToOne
    private Categoria categoria;

    private String descripcion;

    @Column(unique=true)
    private int numeroCaso;

    @OneToOne
    private Prioridad prioridad;

    private String reportante;

    @Temporal(TemporalType.TIMESTAMP)
    private Date technicStartTime;

    @Temporal(TemporalType.TIMESTAMP)
    private Date technicEndTime;

    public boolean isShell() {
        return shell;
    }

    public void setShell(boolean shell) {
        this.shell = shell;
    }

    public String getReportante() {
        return reportante;
    }

    public void setReportante(String reportante) {
        this.reportante = reportante;
    }
    
    public Prioridad getPrioridad() {
        return prioridad;
    }

    public void setPrioridad(Prioridad prioridad) {
        this.prioridad = prioridad;
    }

    public int getNumeroCaso() {
        return numeroCaso;
    }

    public void setNumeroCaso(int numeroCaso) {
        this.numeroCaso = numeroCaso;
    }

    public Categoria getCategoria() {
        return categoria;
    }

    public void setCategoria(Categoria categoria) {
        this.categoria = categoria;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }


    public List<BombaSumergible> getBombaSumergible() {
        return bombaSumergible;
    }

    public void setBombaSumergible(List<BombaSumergible> bombaSumergible) {
        this.bombaSumergible = bombaSumergible;
    }

    public List<Compensador> getCompensador() {
        return compensador;
    }

    public void setCompensador(List<Compensador> compensador) {
        this.compensador = compensador;
    }

    public List<Dispensador> getDispensador() {
        return dispensador;
    }

    public void setDispensador(List<Dispensador> dispensador) {
        this.dispensador = dispensador;
    }

    public List<Elevador> getElevador() {
        return elevador;
    }

    public void setElevador(List<Elevador> elevador) {
        this.elevador = elevador;
    }

    public List<Marquesina> getMarquesina() {
        return marquesina;
    }

    public void setMarquesina(List<Marquesina> marquesina) {
        this.marquesina = marquesina;
    }

    public List<MedidorAire> getMedidorAire() {
        return medidorAire;
    }

    public void setMedidorAire(List<MedidorAire> medidorAire) {
        this.medidorAire = medidorAire;
    }

    public List<Tanque> getTanque() {
        return tanque;
    }

    public void setTanque(List<Tanque> tanque) {
        this.tanque = tanque;
    }

    public SolutionCase getSolutionCase() {
        return solutionCase;
    }

    public void setSolutionCase(SolutionCase solutionCase) {
        this.solutionCase = solutionCase;
    }

    public List<CaseComment> getComentarios() {
        return comentarios;
    }

    public void setComentarios(List<CaseComment> comentarios) {
        this.comentarios = comentarios;
    }

    public Technic getTechnic() {
        return technic;
    }

    public void setTechnic(Technic technic) {
        this.technic = technic;
    }

    public CaseStatus getStatusCase() {
        return statusCase;
    }

    public void setStatusCase(CaseStatus statusCase) {
        this.statusCase = statusCase;
    }

    public List<Surtidor> getSurtidor() {
        return surtidor;
    }

    public void setSurtidor(List<Surtidor> surtidor) {
        this.surtidor = surtidor;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof StationCase)) {
            return false;
        }
        StationCase other = (StationCase) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "controlcasosimirca.persistence.entities.StationCase[id=" + id + "]";
    }

    /**
     * @return the technicStartTime
     */
    public Date getTechnicStartTime() {
        return technicStartTime;
    }

    /**
     * @param technicStartTime the technicStartTime to set
     */
    public void setTechnicStartTime(Date technicStartTime) {
        this.technicStartTime = technicStartTime;
    }

    /**
     * @return the technicEndTime
     */
    public Date getTechnicEndTime() {
        return technicEndTime;
    }

    /**
     * @param technicEndTime the technicEndTime to set
     */
    public void setTechnicEndTime(Date technicEndTime) {
        this.technicEndTime = technicEndTime;
    }
}
