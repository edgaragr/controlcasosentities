/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 *
 * @author edgar
 */
@Entity
public class MarquesinaProductEntry extends AbstractProductEntry implements Serializable {
    @ManyToOne
    private Marquesina marquesina;
    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Marquesina getMarquesina() {
        return marquesina;
    }

    public void setMarquesina(Marquesina marquesina) {
        this.marquesina = marquesina;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof MarquesinaProductEntry)) {
            return false;
        }
        MarquesinaProductEntry other = (MarquesinaProductEntry) object;
        if(this.getProduct().getName().equalsIgnoreCase(other.getProduct().getName())) {
            return true;
        }else{
            return false;
        }
    }

    @Override
    public String toString() {
        return "controlcasosimirca.persistence.entities.MarquesinaEntry[id=" + id + "]";
    }

}
