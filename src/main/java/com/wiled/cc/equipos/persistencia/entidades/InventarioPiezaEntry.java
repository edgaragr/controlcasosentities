/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.wiled.cc.equipos.persistencia.entidades;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

/**
 *
 * @author egarcia
 */
@Entity
public class InventarioPiezaEntry implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @OneToOne
    private Pieza pieza;
    private int count;
    @ManyToOne
    private InventarioPiezas inventarioPiezas;

    public InventarioPiezas getInventarioPiezas() {
        return inventarioPiezas;
    }

    public void setInventarioPiezas(InventarioPiezas inventarioPiezas) {
        this.inventarioPiezas = inventarioPiezas;
    }

    public InventarioPiezaEntry() {
    }

    public InventarioPiezaEntry(Pieza pieza, int count) {
        this.pieza = pieza;
        this.count = count;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public Pieza getPieza() {
        return pieza;
    }

    public void setPieza(Pieza pieza) {
        this.pieza = pieza;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    
}
